import pluginRss from "@11ty/eleventy-plugin-rss";
import pluginSyntaxHighlight from "@11ty/eleventy-plugin-syntaxhighlight";
import pluginNavigation from "@11ty/eleventy-navigation";
import markdownIt from "markdown-it";
import markdownItFootnote from "markdown-it-footnote";
import markdownItAnchor from "markdown-it-anchor";
import markdownItTOC from "markdown-it-table-of-contents";
import markdownItAttrs from "markdown-it-attrs";
import markdownItKatex from "@iktakahiro/markdown-it-katex";
import { isoDate, readableDate, head, min } from "./src/includes/filters/filters.js";
import { tagList, tagListAlphabetically } from "./src/includes/collections/collections.js";

export default function (eleventyConfig) {
    eleventyConfig.addPlugin(pluginRss);
    eleventyConfig.addPlugin(pluginSyntaxHighlight);
    eleventyConfig.addPlugin(pluginNavigation);

    eleventyConfig.addWatchTarget("./src/site/");

    eleventyConfig.addPassthroughCopy("src/site/**/*.svg");
    eleventyConfig.addPassthroughCopy("src/site/**/*.jpg");
    eleventyConfig.addPassthroughCopy("src/site/**/*.jpeg");
    eleventyConfig.addPassthroughCopy("src/site/**/*.jxl");
    eleventyConfig.addPassthroughCopy("src/site/**/*.png");
    eleventyConfig.addPassthroughCopy("src/site/**/*.gif");
    eleventyConfig.addPassthroughCopy("src/site/**/*.webp");
    eleventyConfig.addPassthroughCopy("src/site/**/*.webm");
    eleventyConfig.addPassthroughCopy("src/site/**/*.avif");

    eleventyConfig.setDataDeepMerge(true);
    eleventyConfig.setFrontMatterParsingOptions({
        excerpt: true,
        // Optional, default is "---"
        excerpt_separator: "<!-- excerpt -->"
    });
    eleventyConfig.setLibrary("md", markdownIt({
        html: true,
        linkify: true,
        typographer: false,
    })
        .use(markdownItFootnote)
        .use(markdownItAnchor, {
            permalink: true,
            permalinkBefore: false,
            permalinkClass: "direct-link",
            permalinkSymbol: "",
            level: [1, 2, 3, 4],
        })
        .use(markdownItTOC, {
            includeLevel: [2, 3, 4],
        })
        .use(markdownItAttrs)
        .use(markdownItKatex, {
            output: "html",
        })
    );

    eleventyConfig.addFilter("isoDate", isoDate);
    eleventyConfig.addFilter("readableDate", readableDate);
    eleventyConfig.addFilter("head", head);
    eleventyConfig.addFilter("min", min);

    eleventyConfig.addCollection("tagList", tagList);
    eleventyConfig.addCollection("tagListAlphabetically", tagListAlphabetically);

    return {
        dir: {
            input: "src/site",
            output: "public",
            includes: "../includes",
            layouts: "../includes/templates",
            data: "../data",
        },
    };
}
