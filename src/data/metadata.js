import { DH_NOT_SUITABLE_GENERATOR } from "constants"

const ROOT_URL = "https://blog.mogoh.de/";
const SUBTITLE = "something something technology";
export default {
    lang: "en",
    title: "mogoh’s blog",
    subtitle: SUBTITLE,
    description: SUBTITLE,
    url: ROOT_URL,
    feed: {
        filename: "feed.xml",
        path: "/feed/feed.xml",
        id: ROOT_URL,
    },
    jsonfeed: {
        path: "/feed/feed.json",
        url: ROOT_URL+"feed/feed.json",
    },
    author: {
        name: "mogoh",
        email: "violett-blog@mogoh.de",
        url: ROOT_URL+"imprint/",
    },
}