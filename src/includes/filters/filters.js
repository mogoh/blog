import { DateTime } from "luxon";


export default eleventyConfig =>
    eleventyConfig.addFilter(
        'isoDate',
        function (date) {
            return `${date.toISOString().slice(0, 10)}`;
        },
    );

export function isoDate(dateObj) {
    return DateTime.fromJSDate(dateObj, { zone: 'utc' }).toISODate();
}

export function readableDate(dateObj) {
    return DateTime.fromJSDate(dateObj, { zone: 'utc' }).toFormat("yyyy LLLL dd");
}

// Get the first `n` elements of a collection.
export function head(array, n) {
    if (n < 0) {
        return array.slice(n);
    } else {
        return array.slice(0, n);
    }
}

export function min(...numbers) {
    return Math.min.apply(null, numbers);
}