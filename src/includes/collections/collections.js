export function tagList(collectionAPI) {
    let tagSet = new Set();
    collectionAPI.getAll().forEach(function (item) {
        if ("tags" in item.data) {
            let tags = item.data.tags;
            tags = tags.filter((item) => {
                switch (item) {
                    // Filter all collections, that are not tags
                    // this list should match the `filter` list in tags.njk
                    case "all":
                    case "nav":
                    case "page":
                    case "post":
                    case "posts":
                    case "tagList":
                    case "tagListAlphabetically":
                        return false;
                }
                return true;
            });
            for (const tag of tags) {
                tagSet.add(tag);
            }
        }
    });
    // returning an array in addCollection
    return [...tagSet];
}

export function tagListAlphabetically(collectionAPI) {
    let tag_list = tagList(collectionAPI);
    let sorted_tag_list = tag_list.sort((tag_a, tag_b) => {
        if (tag_a < tag_b) {
            return -1;
        } else if (tag_a > tag_b) {
            return 1;
        } else {
            return 0;
        }
    });
    return sorted_tag_list;
}