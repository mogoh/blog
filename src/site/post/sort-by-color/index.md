---
title: Sort by Color
description: How to choose between different open source licenses.
date: 2021-02-21
tags:
  - game
  - typescript
layout: post.njk
---

I have spend some time, to create a small game, called *Sort by Color*.
It is basically a copy of [I Love Hue](https://play.google.com/store/apps/details?id=com.zutgames.ilovehue).

**[Click here to play it.]({{ metadata.url }}{{ "page/sort-by-color/" | url }})**

## Pick a Puzzle

There are 10,000 puzzles, wich are the seed for the random Generator.
Every level has its own URL, ending with its number.
E. g. URL of puzzle 13 is <{{ metadata.url }}{{ "page/sort-by-color/" | url }}#13>.

![Sort by Color Screenshot; Start Screen](./sort_by_color_1.png "Sort by Color Screenshot; Start Screen")

## Sort by Clicking

Just klick and sort.

![Sort by Color Screenshot; Gameplay](./sort_by_color_2.png "Sort by Color Screenshot; Gameplay")

## Score

The score is the number of sort operations.
Lower is better.

![Sort by Color Screenshot; Score Screen](./sort_by_color_3.png "Sort by Color Screenshot; Score Screen")

Have fun!