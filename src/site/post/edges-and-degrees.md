---
title: Edges and Degrees
description: An exercise about regular polygons
date: 2021-03-15
tags:
  - javascript
  - exercise
layout: post.njk
---

I stumbled apon a small and interesting problem, that I would like to share.

## Problem

We have three regular polygons $A$, $B$, and $C$.
Let $ea$, $eb$, and $ec$ the number of edges of $A$, $B$ and $C$.
Let $dega$ and $degb$ the degree of edges of $A$ and $B$.

What is the number of edges of $A$, $B$, and $C$, if

$ea + eb = ec$ and
$ea < eb$ and
$eb - ea = degb - dega$ and
$ec$ is a square number.


## Code for Calculation

```js
function inner_degree(n) {
    return ((n - 2) * 180) / n;
}

let c_sqrt = 3;
let found = false;

while (!found) {
    let c = c_sqrt * c_sqrt;
    let a = 3;
    while (a < c / 2) {
        let b = c - a;
        let inner_degree_a = inner_degree(a);
        let inner_degree_b = inner_degree(b);
        let degree_delta = inner_degree_b - inner_degree_a;
        let edge_delta = b - a;
        if (degree_delta === edge_delta) {
            console.log("A: ", a);
            console.log("Inner degree: ", inner_degree_a);
            console.log("B: ", b);
            console.log("Inner degree: ", inner_degree_b);
            console.log("Degree Delta: ", degree_delta);
            console.log("Edge Delta: ", edge_delta);
            console.log("C: ", c);
            console.log("SQRT: ", c_sqrt);
            found = true;
        }
        a += 1;
    }
    c_sqrt += 1;
}
```

## Solution

$A$ has $9$ edges with an inner angle of $140°$.
$B$ has $40$ edges with an inner angle of $171°$.
The delta of inner angles is equal to the delta of eges: $31$.
$C$ has $49$ edges witch is a square number.

Degree Delta:  $31$  
Edge Delta:  $31$  
C:  $49$  
SQRT:  $7$  
