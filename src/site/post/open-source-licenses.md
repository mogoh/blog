---
title: Open Source Licenses
description: How to choose between different open source licenses.
date: 2021-02-11
tags:
  - open-source
  - free-software
  - licenses
layout: post.njk
---

[[toc]]

As of today, there are many, many open source licenses out in the wild.
Wikipedia^[https://en.wikipedia.org/wiki/Category:Free_and_open-source_software_licenses] lists 89 pages (as of writing).
GNU lists also a lot.^[https://www.gnu.org/licenses/license-list.en.html]
Let’s get some help, to choose the right one.
<!-- excerpt -->

## License Abbreviation

“GPL”, “MIT”, and abbreviations as such are standardized by now by SPDX^[https://spdx.org/licenses/].

## Choosing

A good page for starters is choosealicense.com.^[https://choosealicense.com/]
Licenes are sorted by the permissions they grant.

### No License

No License means no permissions granted.
This is the opposite of free.

### Public Domain, Unlicense, CC0, and similar

Public Domain is not available in every country.
Thus an alternative permissive license is requried.
choosealicense suggests the “Unlicense” for that case.^[https://choosealicense.com/licenses/unlicense/]
GNU on the other hand suggests to pick the CC0^[https://creativecommons.org/publicdomain/zero/1.0/legalcode] over the Unlicense:

> If you want to release your work to the public domain, we recommend you use CC0. CC0 also provides a public domain dedication with a fallback license, and is more thorough and mature than the Unlicense.
^[https://www.gnu.org/licenses/license-list.en.html#Unlicense]

This goes even if other licenses of Creative Commons are not recomended for software.^[https://creativecommons.org/faq/#can-i-apply-a-creative-commons-license-to-software]
CC0 is an exception.^[https://wiki.creativecommons.org/wiki/CC0_FAQ#May_I_apply_CC0_to_computer_software.3F_If_so.2C_is_there_a_recommended_implementation.3F]
