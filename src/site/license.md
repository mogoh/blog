---
layout: main.njk
title: License
description: Under which license is the content of this page published?
---
# License

Written in 2021 by Leonhard Küper alias mogoh.

All content, if not marked differently, is licensed unter Creative Commons Attribution 4.0 International (CC-BY-4.0).
See <https://creativecommons.org/licenses/by/4.0/legalcode> for details.