---
title: WebDev Checklist
layout: main.njk
tags: [
    "page",
]
---
# WebDev Checklist
A checklist for better web development.

## HTML

* meta
  * \<meta charset="utf-8">
  * \<meta name="viewport" ontent="width=device-width, initial-scale=1.0">
  * \<meta name="description" content="">
  * \<link rel=preload> https://web.dev/uses-rel-preload/
* performance
  * img loading="lazy" https://web.dev/browser-level-image-lazy-loading/
  * iframe loading="lazy" https://developer.mozilla.org/en-US/docs/Web/Performance/Lazy_loading
  * content-visibility https://developer.mozilla.org/en-US/docs/Web/CSS/content-visibility
* favicon & co
* Validator
  * https://validator.w3.org/

## Structured Data

* JSON-LD
  * https://schema.org/
  * https://search.google.com/structured-data/testing-tool/
  * https://developers.google.com/search/docs/guides/intro-structured-data

## CSS

* reset
  * rem
  * box-sizing
  * text-size-adjust
* @media
  * printer-friendly
  * darkmode/lightmode
  * desktop
  * mobile
  * prefers-reduced-motion
* colors
  * background-color
  * color
  * :focus
* font
  * typeface
  * weight
  * style
  * font-display: swap; https://web.dev/font-display/
* typograpyh
  * paragraph
  * headline
  * teaser
  * kicker
  * hypens
  * size/width
  * font-variant-ligatures: common-ligatures;
  * font-variant-ligatures: discretionary-ligatures;
  * text-indent
  * hanging-punctuation
* overflow/text-overflow
* caret-color
* tab-size
* elements
  * content
    * address
    * article
    * aside
    * footer
    * header
    * h1, h2, h3, h4, h5, h6
    * main
    * nav
    * section
  * text
    * a
      * markers
      * scroll-margin-top
    * abb
    * abbr
    * b
    * blockquote
    * br
    * cide
    * data
    * del
    * dfn
    * div
    * dl/dd/dt
    * em
    * figure/fiurecaption
    * hr
    * i
    * ins
    * kbd
    * mark
    * p
    * pre
    * q
    * s
    * samp
    * small
    * span
    * strong
    * time
    * u
    * ul/ol/li
    * var
  * media
    * img
    * audio
    * video
    * svg
    * math
  * table
    * ...
  * form
    * ...
  * details/sumary
* code
  * maybe add prismjs
* Validator https://jigsaw.w3.org/css-validator/ (results may be false positive)

## Accessibility

* https://www.w3.org/WAI/standards-guidelines/wcag/
* https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Cross_browser_testing/Accessibility
* https://achecker.ca/checker/index.php
* https://webaim.org/resources/contrastchecker/

### ARIA

* https://accessibility.oit.ncsu.edu/using-aria-landmarks-a-demonstration/
* http://www.punkchip.com/aria-basic-findings/

## Validator

* Link Checker https://validator.w3.org/checklink
* I18n Checker https://validator.w3.org/i18n-checker/

## Other

* http://humanstxt.org/
