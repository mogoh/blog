import { PRNG } from "./PRNG.js";


interface Container {
    position: number,
    cell: HTMLElement,
    element: {
        pinned: boolean,
        field: HTMLElement,
        origin: number,
    },
}

class Game {
    WIDTH!: number;
    HEIGHT!: number;
    SIZE!: number;
    virtualBoard!: Array<Container>;
    a: Container | undefined;
    b: Container | undefined;
    score = 0;
    prng: PRNG;
    seed: number
    topColor!: number[];
    rightColor!: number[];
    bottomColor!: number[];
    leftColor!: number[];

    constructor(seed: number) {
        this.seed = seed;
        this.prng = new PRNG(seed);
    }

    setSeed(seed: number) {
        this.seed = seed;
        this.prng = new PRNG(seed);
    }

    start() {
        this.score = 0;
        this.shuffel();
    }

    numberToPosition(n: number): number[] {
        return [
            Math.floor(n / this.WIDTH),
            n % this.WIDTH,
        ];
    }

    generateBoard() {
        this.WIDTH = this.prng.nextInt(3, 8);
        this.HEIGHT = this.prng.nextInt(3, 8);
        this.SIZE = this.WIDTH * this.HEIGHT;
        GAME_BOARD.innerHTML = "";
        this.virtualBoard = new Array<Container>(this.SIZE);
        this.topColor = [this.prng.nextInt(0, 240), this.prng.nextInt(0, 120), this.prng.nextInt(0, 100)];
        this.rightColor = [(this.topColor[0] + this.prng.nextInt(50, 100)) % 360, this.prng.nextInt(0, 100), this.prng.nextInt(0, 100)];
        this.bottomColor = [(this.topColor[0] + this.prng.nextInt(50, 100)) % 360, this.prng.nextInt(0, 100), this.prng.nextInt(0, 100)];
        this.leftColor = [(this.topColor[0] + this.prng.nextInt(50, 100)) % 360, this.prng.nextInt(0, 100), this.prng.nextInt(0, 100)];

        let row = document.createElement("board-row");
        for (let i = 0; i < this.SIZE; i += 1) {
            // ROWS
            if (i % this.WIDTH === 0) {
                row = document.createElement("board-row");
                GAME_BOARD.append(row);
            }

            const cell = document.createElement("board-cell");
            cell.style.height = `${100 / this.HEIGHT}vh`;
            cell.style.width = `${100 / this.WIDTH}vw`;
            row.append(cell);

            const field = document.createElement("board-field");
            const color = this.setHSL(i);
            field.style.backgroundColor = color;
            field.style.height = `${100 / this.HEIGHT}vh`;
            field.style.width = `${100 / this.WIDTH}vw`;
            // field.innerHTML = `Origin: [${i}],<br>${color}`;

            let pinned = false;
            if (
                i === 0
                || i === this.WIDTH - 1
                || i === this.SIZE - this.WIDTH
                || i === this.SIZE - 1
            ) {
                pinned = true;
            }

            if (pinned) {
                const pin = document.createElement("field-pin");
                field.append(pin);
            }

            if (!pinned) {
                cell.addEventListener(
                    "click",
                    (e) => {
                        this.selectField(e, this.virtualBoard[i]);
                    },
                );
            }

            this.virtualBoard[i] = {
                position: i,
                cell: cell,
                element: {
                    origin: i,
                    field: field,
                    pinned: pinned,
                }
            };
            cell.append(field);
        }
    }

    selectField(e: Event, container: Container) {
        if (this.a === undefined) {
            this.a = container;
            container.element.field.className = "selected";
        } else if (this.a.position === container.position) {
            this.a.element.field.className = "";
            this.a = undefined;
        } else {
            this.b = container;
            this.score += 1;
            this.swap(this.a, this.b);
            this.check();
            this.a.element.field.className = "";
            this.b.element.field.className = "";
            this.a = undefined;
            this.b = undefined;
        }
    }

    swap(a: Container, b: Container) {
        a.cell.removeChild(a.element.field);
        b.cell.removeChild(b.element.field);

        const temp = a.element;
        a.element = b.element;
        b.element = temp;

        a.cell.append(a.element.field);
        b.cell.append(b.element.field);
    }

    shuffel() {
        let currentIndex = this.SIZE - 1;
        while (currentIndex !== 0) {
            if (!this.virtualBoard[currentIndex].element.pinned) {
                let randomIndex = 0;
                while (this.virtualBoard[randomIndex].element.pinned
                    || randomIndex === currentIndex
                ) {
                    randomIndex = this.prng.nextInt(0, this.SIZE - 1);
                }
                this.a = this.virtualBoard[randomIndex];
                this.b = this.virtualBoard[currentIndex];
                this.swap(this.a, this.b);
            }
            currentIndex -= 1;
        }
        this.a = undefined;
        this.b = undefined;
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    setRGB(i: number): string {
        const [row, column] = this.numberToPosition(i);
        const i_frac = (row + 1) / this.HEIGHT;
        const j_frac = (column + 1) / this.WIDTH;
        const r = Math.floor(i_frac * 255);
        const g = Math.floor((j_frac + i_frac) / 2 * 255);
        const b = Math.floor(j_frac * 255);
        // rgb(0-255, 0-255, 0-255);
        return `rgb(${r},${g},${b})`;
    }

    setHSL(i: number): string {
        const [row, column] = this.numberToPosition(i);
        const rightPart = .5 * column / (this.WIDTH - 1);
        const leftPart = .5 - rightPart;
        const bottomPart = .5 * row / (this.HEIGHT - 1);
        const topPart = .5 - bottomPart;
        // console.log(row, column, topPart);

        const hue = Math.floor(
            this.topColor[0] * topPart +
            this.rightColor[0] * rightPart +
            this.bottomColor[0] * bottomPart +
            this.leftColor[0] * leftPart
        );
        const saturation = Math.floor(
            this.topColor[1] * topPart +
            this.rightColor[1] * rightPart +
            this.bottomColor[1] * bottomPart +
            this.leftColor[1] * leftPart
        );
        const lightness = Math.floor(
            this.topColor[2] * topPart +
            this.rightColor[2] * rightPart +
            this.bottomColor[2] * bottomPart +
            this.leftColor[2] * leftPart
        );
        // hsl(360, 100%, 50%)
        // return `hsl(${hue}, ${saturation}%, ${lightness}%)`;
        return `hsl(${hue}, ${saturation}%, ${lightness}%)`;
    }

    check() {
        let valid = true;
        for (const el of this.virtualBoard) {
            if (el.position !== el.element.origin) {
                valid = false;
            }
        }
        if (valid) {
            scoreScreen();
        }
    }
}

const GAME_BOARD = document.querySelector(".game-board") as HTMLElement;
const START_SCREEN = document.querySelector(".start-screen") as HTMLElement;
const SCORE_SCREEN = document.querySelector(".score-screen") as HTMLElement;

const potential_seed = window.location.hash.substr(1);
let seed = parseInt(potential_seed);
if (isNaN(seed) || seed < 1 || 10_000 < seed) {
    seed = Math.floor((Math.random() * 10_000) + 1);
    window.location.hash = `#${seed}`;
}
const GAME = new Game(seed);


function startScreen() {
    function regenerate() {
        seed = parseInt(input.value);
        window.location.hash = `#${seed}`;
        GAME.setSeed(seed);
        GAME.generateBoard();
    }

    START_SCREEN.style.display = "initial";

    const input = document.querySelector(".start-screen input") as HTMLInputElement;
    const button = document.querySelector(".start-screen button") as HTMLInputElement;
    input.value = seed.toString();

    input.addEventListener("change", regenerate);
    input.addEventListener("input", regenerate);
    button.addEventListener("click", () => {
        START_SCREEN.style.display = "none";
        GAME.start();
    });

    GAME.generateBoard();
}

function scoreScreen() {
    SCORE_SCREEN.style.display = "initial";

    const button = document.querySelector(".score-screen button") as HTMLInputElement;
    const scoreField = document.querySelector(".score-screen .score-number") as HTMLElement;
    scoreField.innerText = `${GAME.score}`;
    const puzzleField = document.querySelector(".score-screen .puzzle-number") as HTMLElement;
    puzzleField.innerText = `${GAME.seed}`;

    button.addEventListener("click", () => {
        SCORE_SCREEN.style.display = "none";
        seed = Math.floor((Math.random() * 10_000) + 1);
        window.location.hash = `#${seed}`;
        startScreen();
    });
}

startScreen();
