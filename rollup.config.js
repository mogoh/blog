import resolve from '@rollup/plugin-node-resolve';
import { default as importHTTP } from 'import-http/rollup';
import typescript from '@rollup/plugin-typescript';
import { terser } from 'rollup-plugin-terser';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import multiInput from 'rollup-plugin-multi-input';


const isProduction = process.env.NODE_ENV === 'production'


const dev = {
    input: [ './src/site/**/*.ts' ],
    output: {
        dir: 'public/',
        format: 'esm',
        entryFileNames: '[name].js',
        sourcemap: 'inline',
    },
    plugins: [
        multiInput({
            relative: 'src/site/',
        }),
        nodeResolve(),
        typescript(),
        resolve(),
        importHTTP(),
    ],
    watch: {
        exclude: ['node_modules/**'],
    },
}

const prod = {
    input: [ './src/site/**/*.ts' ],
    output: {
        dir: 'public/',
        format: 'esm',
        sourcemap: false,
        compact: true,
        minifyInternalExports: true,
        entryFileNames: '[name].js',
    },
    plugins: [
        multiInput({
            relative: 'src/site/',
        }),
        nodeResolve(),
        typescript({ inlineSourceMap: false }),
        resolve(),
        importHTTP(),
        terser(),
    ],
}

export default isProduction
    ? prod
    : dev;
