# README

A personal website build with this technologies:

* <https://www.11ty.dev/>
* <https://www.typescriptlang.org/>
* <https://rollupjs.org/>
* <https://postcss.org/>
* <https://nodejs.org/>
* <https://eslint.org/>
* <https://yarnpkg.com/>
* and many more ... take a look at `package.json`

## Enhancements

* add structured data json-ld
* Markdown
  * subscript
  * superscript
  * definition list
  * abbreviation
  * emoji
  * custom container
  * insert
  * mark
  * https://www.npmjs.com/package/markdown-it-multimd-table
  * syntax highlighting
* html2pdf
* Pass lighthouse Tests
  * <https://web.dev/tap-targets/?utm_source=lighthouse&utm_medium=devtools>
* Pass Lighthouse PWA Test
* Hide pages from indexing (robots.txt) and sitemap
* View the sourcecode on gitlab link https://www.11ty.dev/docs/quicktips/edit-on-github-links/

