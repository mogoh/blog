const postcssPresetEnv = require('postcss-preset-env');
const postcssImport = require('postcss-import');
const importUrl = require('postcss-import-url');
const cssnano = require('cssnano');
const url = require("postcss-url");


const isProduction = process.env.NODE_ENV === 'production'


const dev = {
    plugins: [
        importUrl(),
        postcssImport(),
        postcssPresetEnv({
            stage: 0,
        }),
        url({
            url: 'copy',
            assetsPath: '.',
        }),
    ],
}

const prod = {
    plugins: [
        importUrl(),
        postcssImport(),
        postcssPresetEnv({
            stage: 0,
        }),
        cssnano({
            preset: 'default',
        }),
        url({
            url: 'copy',
            assetsPath: '.',
        }),
    ],
}

module.exports = isProduction
    ? prod
    : dev;
